<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=0">
<meta name="description"
	content="Frest admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords"
	content="admin template, Frest admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="PIXINVENT">
<title>Handyman</title>
<link rel="apple-touch-icon"
	href="../app-assets/images/ico/apple-icon-120.png">
<link rel="shortcut icon" type="image/x-icon"
	href="../app-assets/images/ico/favicon.ico">
<link
	href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700"
	rel="stylesheet">

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css"
	href="../app-assets/vendors/css/vendors.min.css">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/colors.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/components.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/themes/dark-layout.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/themes/semi-dark-layout.css">

<link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.css">

<link rel="stylesheet" type="text/css" href="assets/css/style.css">

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/datatables.min.css">


<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
<link rel="stylesheet" type="text/css" 	href="../app-assets/vendors/css/pickers/daterange/daterangepicker.css">

</head>


<body
	class="vertical-layout vertical-menu-modern boxicon-layout no-card-shadow 2-columns  navbar-sticky footer-static  "
	data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">



	<!-- BEGIN: Content-->
	<div class="../app-content content">
		<div class="content-overlay"></div>
		<div class="content-wrapper">
			<div class="content-header row">
				<div class="content-header-left col-12 mb-2 mt-1">
					<div class="row breadcrumbs-top">
						<div class="col-12">

							<div class="breadcrumb-wrapper col-12">
								<ol class="breadcrumb p-0 mb-0">
									<li class="breadcrumb-item"><a href="/dashboard"><i
											class="bx bx-home-alt"></i></a></li>
									<li class="breadcrumb-item"><a href="#">Reports
											Management</a></li>
									<li class="breadcrumb-item active">Complaint Report</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="content-body">
				<!-- Basic Modals start -->




				<!-- Form and scrolling Components end -->
				<section class="users-list-wrapper">
					
					<div class="users-list-table">
						<div class="card">
							<div class="card-content">
								<div class="card-body">
									<!-- datatable start -->
									<div class="table-responsive">
										<table id="users-list-datatable" class="table">
											<thead>
												<tr>
													<th>S No</th>
													<th>User</th>
													<th>Provider</th>
													<th>Service category</th>
													<th>Service name</th>
													<th>Description</th>
													<th>Image</th>
													<th>Video</th>
													
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>Kuldeep</td>
													<td>Raju</td>
													<td>Cleaning</td>
													<td>Kitchen Cleaning</td>
													<td>Very bad</td>
													<td>Nil</td>
													<td>Nil</td>
													
												</tr>
												<tr>
													<td>2</td>
													<td>Vikram</td>
													<td>Ravi</td>
													<td>Cleaning</td>
													<td>Home Cleaning</td>
													<td>Good</td>
													<td>Nil</td>
													<td>Nil</td>
													
												</tr>


											</tbody>
										</table>
									</div>
									<!-- datatable ends -->
								</div>
							</div>
						</div>
					</div>
				</section>


			</div>
		</div>
	</div>
	<!-- END: Content-->


	<div class="sidenav-overlay"></div>
	<div class="drag-target"></div>




	<script src="../app-assets/vendors/js/vendors.min.js"></script>
	<script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
	<script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
	<script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
	<script src="../app-assets/js/core/../app-menu.js"></script>
	<script src="../app-assets/js/core/app.js"></script>
	<script src="../app-assets/js/scripts/components.js"></script>
	<script src="../app-assets/js/scripts/footer.js"></script>
	<script src="../app-assets/js/scripts/modal/components-modal.js"></script>
	<script src="../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
	<script
		src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
	<script src="../app-assets/js/scripts/pages/page-users.js"></script>
	<script src="../app-assets/vendors/js/pickers/pickadate/picker.js"></script>
	<script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
	<script src="../app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
	<script src="../app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
	<script src="../app-assets/vendors/js/pickers/daterange/moment.min.js"></script>
	<script
		src="../app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
	<script src="../app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>




</body>
<!-- END: Body-->

</html>